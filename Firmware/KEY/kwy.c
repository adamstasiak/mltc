/*
 * kwy.c
 *
 *  Created on: 28 pa� 2017
 *      Author: Adam
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "key.h"


void key_init(void){

	DDRC &= ~(KEY0|KEY1);
}
uint8_t which_pressed(uint8_t which){

	if( !(PINC & which)){
		_delay_ms(80);
		if( !(PINC & which)){
			_delay_ms(80);
			return 1;
		}
	}
	return 0;
}
