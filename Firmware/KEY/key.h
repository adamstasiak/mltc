/*
 * key.h
 *
 *  Created on: 28 pa� 2017
 *      Author: Adam
 */

#ifndef KEY_KEY_H_
#define KEY_KEY_H_

#define KEY1 (1<<PC6)
#define KEY0 (1<<PC7)

uint8_t which_pressed(uint8_t which);
void key_init(void);

#endif /* KEY_KEY_H_ */
