/*
 * time.h
 *
 *  Created on: 28 pa� 2017
 *      Author: Adam
 */

#ifndef TIME_TIME_H_
#define TIME_TIME_H_

enum {csec, sec, min, hou, year_date, weekdays_months};
enum {sekundy, minuty, godziny, rok, dzien, dzienTygodnia, miesiac};

extern volatile uint8_t int0Flag;
void time_init();

#endif /* TIME_TIME_H_ */
