/*
 * time.c
 *
 *  Created on: 28 pa� 2017
 *      Author: Adam
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "time.h"
#include "../KEY/key.h"
#include "../LCD/lcd44780.h"

volatile uint8_t int0Flag=0;

uint8_t timeCounter[3];

void time_init(){

	// Przerwanie INT0
		MCUCR |= (1<<ISC01);	// wyzwalanie zboczem opadaj�cym
		GICR |= (1<<INT0);		// odblokowanie przerwania
}


ISR( INT0_vect ) {
	int0Flag = 1;
}
