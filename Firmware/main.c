/*
 * main.c
 *
 *  Created on: 30 sie 2017
 *      Author: Adam Stasiak
 */


#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/wdt.h>

#include "LCD/lcd44780.h"
#include "TIME/time.h"
#include "KEY/key.h"
#include "7_SEG/led.h"

#define INPUT (1<<PD3)
#define INPUT_R (1<<PC0)
#define BUZZER (1<<PC1)
#define BUZZER_ON PORTC |= BUZZER
#define BUZZER_OFF PORTC &= ~BUZZER
#define BUZZER_TOG PORTC ^= BUZZER
#define LED_LAYOVER (1<<PD0)
#define LED_LAYOVER_OFF PORTD &= ~LED_LAYOVER
#define LED_LAYOVER_ON PORTD |= LED_LAYOVER
#define LED_LAYOVER_TOG PORTD ^= LED_LAYOVER
#define LED_MOTION (1<<PD1)
#define LED_MOTION_OFF PORTD &= ~LED_MOTION
#define LED_MOTION_ON PORTD |= LED_MOTION
#define LED_MOTION_TOG PORTD ^= LED_MOTION


uint16_t timeCounter[3] = {0,0,0};
volatile uint8_t timeToStandby=0;

void display_counted_time(void);


int main (void){

	DDRA |= (1<<PA0);
	PORTA &= ~(1<<PA0);

	DDRD |= (LED_LAYOVER|LED_MOTION);
	PORTD &= ~(LED_LAYOVER|LED_MOTION);

	DDRC |= BUZZER;
	DDRC &= ~INPUT_R;
	PORTC |= INPUT_R;
	PORTC &= ~BUZZER;

	DDRD &= ~INPUT;

	d_led_init();
	time_init();
	key_init();
	lcd_init();

	sei();

	lcd_cls();

	uint16_t layoverTime[3] = {0,0,0};
	uint16_t motionTime[3] = {0,0,0};

	uint8_t lastInput = 0;
	uint8_t currentInput = 0;

	uint8_t lastInputR = 0;
	uint8_t currentInputR = 0;

	uint8_t count=0;
	uint8_t alarm=0;
	uint8_t motionOrLayover=0;

	uint8_t signalCounter=0;

	lastInput = PIND & INPUT;
	lastInputR = PINC & INPUT_R;

	while (1){

		wdt_enable( WDTO_1S );

		LED_LAYOVER_OFF;
		LED_MOTION_OFF;

		if(int0Flag){

			int0Flag=0;
		}


		PORTA &= ~(1<<PA0);
		PORTD &= ~(CA1 | CA2 | CA3 | CA4);

		if(which_pressed(KEY1)){

			LED_LAYOVER_OFF;
			LED_MOTION_OFF;
			PORTA |= (1<<PA0);
			count=1;
		}

		lastInput = PIND & INPUT;
		lastInputR = PINC & INPUT_R;

		while(count){

			wdt_enable( WDTO_1S );

			lcd_locate(0,0);
			lcd_str("T.post.:");
			lcd_locate(1,0);
			lcd_str("T.ruch.:");

			currentInput = PIND & INPUT;
			currentInputR = PINC & INPUT_R;

			if(int0Flag){

				timeCounter[sekundy]++;

				if((!(timeCounter[godziny]%4)) && (timeCounter[godziny]!=0) && (timeCounter[minuty]<1) && (motionTime[sekundy]<21) && (alarm == 0) && (motionOrLayover)){

					BUZZER_TOG;
					LED_MOTION_TOG;
					LED_LAYOVER_TOG;
				}else{

					BUZZER_OFF;
					LED_MOTION_OFF;
					LED_LAYOVER_OFF;
				}

				signalCounter++;

				int0Flag=0;
			}

			if(which_pressed(KEY1))alarm=1;
			if(timeCounter[sekundy]>21) alarm=0;

			if(currentInput == 0 && lastInput != 0 && currentInputR != 0){

				if (motionOrLayover ==0){
					signalCounter=0;
				}
				motionOrLayover =1;

				for(uint8_t i=0;i<3;i++)timeCounter[i]=0;
			}
			if(currentInputR != 0 && lastInputR == 0 && currentInput == 0){

				if (motionOrLayover == 0){
					signalCounter=0;
				}
				motionOrLayover =1;

				for(uint8_t i=0;i<3;i++)timeCounter[i]=0;
			}
			if(currentInput != 0 && lastInput == 0){

				if (motionOrLayover){
					signalCounter=0;
				}
				motionOrLayover =0;
				for(uint8_t i=0;i<3;i++)timeCounter[i]=0;
			}

			if(motionOrLayover && signalCounter>2){

				LED_MOTION_ON;
				LED_LAYOVER_OFF;
				motionTime[sekundy]=timeCounter[sekundy];
				signalCounter=4;
			}else if(signalCounter>2){

				LED_MOTION_OFF;
				LED_LAYOVER_ON;
				layoverTime[sekundy]=timeCounter[sekundy];
				signalCounter=4;
			}

			if(motionTime[sekundy]>59)motionTime[minuty]++, motionTime[sekundy]=0;
			if(motionTime[minuty]>59)motionTime[godziny]++, motionTime[minuty]=0;

			if(layoverTime[sekundy]>59)layoverTime[minuty]++, layoverTime[sekundy]=0;
			if(layoverTime[minuty]>59)layoverTime[godziny]++, layoverTime[minuty]=0;

			if(timeCounter[sekundy]>59)timeCounter[minuty]++, timeCounter[sekundy]=0;
			if(timeCounter[minuty]>59)timeCounter[godziny]++, timeCounter[minuty]=0;

			if(layoverTime[godziny]>99){

				lcd_locate(0,9);
				lcd_int(layoverTime[godziny]);
				lcd_str("h   ");
			}else{

				lcd_locate(0,9);
				lcd_int(layoverTime[godziny]);
				lcd_str("h:");
				lcd_int(layoverTime[minuty]);
				lcd_str("m");
			}

			if(motionTime[godziny]>99){

				lcd_locate(1,9);
				lcd_int(motionTime[godziny]);
				lcd_str("h   ");
			}else{

				lcd_locate(1,9);
				lcd_int(motionTime[godziny]);
				lcd_str("h:");
				lcd_int(motionTime[minuty]);
				lcd_str("m");
			}

			display_counted_time();

			if(which_pressed(KEY0)){

				timeToStandby=1;

				while(timeToStandby<121){

					wdt_enable( WDTO_1S );

					if(which_pressed(KEY1))timeToStandby = 250, count=1;
					else count=0;

					PORTD &= ~(CA1 | CA2 | CA3  | CA4);
					LED_LAYOVER_ON;
					LED_MOTION_ON;
					BUZZER_OFF;

					if(int0Flag){
						timeToStandby++;
						int0Flag=0;
					}

					wdt_reset();
				}

					for(uint8_t i=0;i<3;i++){
						timeCounter[i]=0;
						motionTime[i]=0;
						layoverTime[i]=0;
					}

				lcd_cls();
			}

			lastInput = currentInput;
			lastInputR = currentInputR;
			wdt_reset();
		}

		wdt_reset();
	}
}
void display_counted_time(void){

	if(timeCounter[minuty]<10)cy3 = 0, cy4 = timeCounter[minuty];
	else cy3= timeCounter[minuty] / 10, cy4 = timeCounter[minuty] % 10;

	if(timeCounter[godziny]<10)cy1 = 0, cy2 = timeCounter[godziny];
	else cy1 = timeCounter[godziny] / 10, cy2 = timeCounter[godziny] % 10;
}
